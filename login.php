<?php 
	include'includes/header.php';
	include'includes/functions.inc.php'; 

	//if they're already logged in and came to the login page, redirect them to the home page
		if (!empty($_SESSION))
			echo ('<script type="text/javascript">window.location = "home.php";</script>');
?>

<div id='content' style='height:400px'>
	<?php 
		//login attempt
		if (isset($_POST['loginEmail'])){
			$loggedIn = login($_POST);
			//redirect to home page on successful login
			if ($loggedIn)
				echo ('<script type="text/javascript">window.location = "home.php";</script>');
		}
	?>

	<div style='margin:auto;width:285px;'>
		<?php echo($loginHTML);?>
	</div>
</div>

<?php include'footer.php';?>