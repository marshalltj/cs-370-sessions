<?php
    include 'includes/header.php';
    include 'includes/functions.inc.php';
?>

<div id='content'>
    <h1>Create an Account</h1>
    <?php
        if (!empty($_POST)){
            registerUser($_POST);
        }
        else{
            echo $registerHTML;
        }
    ?>
</div>

<?php include 'includes/footer.php';?>