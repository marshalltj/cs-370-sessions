<?php
    include 'includes/header.php';
    include 'includes/functions.inc.php';
	//if we're not logged in redirect
	if (empty($_SESSION))
		echo ('<script type="text/javascript">window.location = "login.php";</script>');

	//if we're not viewing a class redirect
	if (empty($_GET))
		echo ('<script type="text/javascript">window.location = "home.php";</script>');

	//they submitted a new post
	if (!empty($_POST))
		if (isset($_POST['reply']))
			submitReply();
		if (isset($_POST['deletePost']))
			deletePost();
?>

<div id='content' style='min-height:500px;'>
	<h1 style='margin-bottom:0px;'>View Post</h1>
	<?php 
		postHTML();
	?>
</div>

<?php include'includes/footer.php';?>
