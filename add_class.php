	<?php 
	include'includes/header.php'; 
	include'includes/functions.inc.php'; 	
	
	//if we're not logged in redirect
	if (empty($_SESSION))
			echo ('<script type="text/javascript">window.location = "login.php";</script>');
?>

<div id='content' style='height:auto;'>
	<h1>Add/Browse Classes</h1>
	<?php 
		// if the post is empty, we show the search stuff
		if (empty($_POST))
			searchClassHTML($_GET);
		else
			addClass($_POST['classID']);
	?>

</div>

<?php include'includes/footer.php';?>