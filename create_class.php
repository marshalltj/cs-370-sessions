	<?php 
	include'includes/header.php'; 
	include'includes/functions.inc.php'; 	
	
	//if we're not logged in redirect
	if (empty($_SESSION))
			echo ('<script type="text/javascript">window.location = "login.php";</script>');
?>

<div id='content'>
	<h1>Create a Class</h1>
	<div style="margin:auto;width:300px;">
		<?php 
			if (empty($_POST))
				echo $createClassHTML;
			else
				createClass($_POST);
			
		?>
	</div>

</div>

<?php include'includes/footer.php';?>