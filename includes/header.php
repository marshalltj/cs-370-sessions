<head>
<title>Sessions</title>
 <link rel="stylesheet" type="text/css" href="style.css">
<link rel="icon" type="image/png" href="img/favicon.png"/>
</head>

<?php 
	//begin our sessions
	ob_start(); 
	session_start();
	$msc=microtime(true);
	//decide what page the logo links to
	if (!empty($_SESSION) && $_SERVER['REQUEST_URI'] != '/~jankovsk/cs370/logout.php')
		$logoURL = 'home.php';
	else
		$logoURL = 'index.php';

	//homepage display vs other pages
	if ($_SERVER['REQUEST_URI'] != '/~jankovsk/cs370/index.php' && $_SERVER['REQUEST_URI'] != '/~jankovsk/cs370/' )
		echo "<body>";
	else
		echo "<body id='homeBody'>";
?>

<div id='header'>
	<div id='greyBox'>
		<?php echo("<a href='$logoURL'>");?>
	  <img src="img/header/logo.png" style="position: relative; left: 20px;"></a>
	  <?php echo("<span id='headerSpan' style='left:25px;bottom:75px'>" . date('l\, jS \of F Y') . "</span>");?>
	  <span>
	  	<?php 
	  		//begin our sessions
	  		ob_start(); 
				session_start();
	  		//either display a login button or a my account/logout info
				if (!empty($_SESSION) && $_SERVER['REQUEST_URI'] != '/~jankovsk/cs370/logout.php'){
					echo("
						<img src='img/profile/" . $_SESSION['picURL'] . "' style='float:right; width:90px; margin:10px; box-shadow:-1px 2px 5px grey;'>
						<span id='headerSpan' style='float:right; top:25px;'>
							<center>Hello, " . explode(',', $_SESSION['username'])[0] . "</center>
							<a href='#'>My Account</a> | <a href='logout.php'>Logout</a>
						</span>
					");

					//navigation
					echo("
					");

				}
	  	?>
  </div>
</div>
<br>
