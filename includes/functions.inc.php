<?php
include'sql.inc.php';
// Report all PHP errors
error_reporting(-1);

//html Templates

$loginHTML = "
	<form action='login.php' method='post' id='loginForm'>
		<label>Email</label>
		<input type='email' name='loginEmail' required><br>
		<label>Password</label>
		<input type='password' name='loginPW' required><br><br>
		<input type='submit' id='buttonStyle' style='margin-left:70px;' value='Login'>
	</form>
";

$createClassHTML ="
	<form action='create_class.php' method='post' id='createForm'>
		<label>Course Subject</label>
		<input type='text' name='courseSubject' required><br>
		<label>Course Number</label>
		<input type='text' name='courseNumber' required><br>
		<label>Section</label>
		<input type='text' name='section' required><br>
		<label>Professor</label>
		<input type='text' name='prof' required><br><br>
		<input type='submit' id='buttonStyle' style='margin-left:50px' value='Create Class'>
	</form>
";
    
$registerHTML = "
  <form action='register.php' id='regForm' method='post'>
      <label>First Name:</label>
      <input type='text' name='fName' required><br>
      <label>Last Name:</labeL>
      <input type=text name='lName' required><br>
      <label>Email:</label>
      <input type='email' name='email' required><br>
      <label>Confirm Email:</label>
      <input type='email' id='confirmEmail' required><br>
      <label>Password:</label>
      <input type='password' name='password' id='password' required><br>
      <label>Confirm Password:</label>
      <input type='password' id='confirmPassword' required><br><br>
      <input type='submit' id='buttonStyle' style='margin-left:30px;' value='Create Account'>
  </form>
";

//login function. Returns true if sucessful login
function login($post){
	//grabbing relavent post info
	$email = $post['loginEmail']; $pw = $post['loginPW'];

	//getting user from database
	$con = sql_connect();
	$query = mysqli_query($con, "SELECT * FROM users WHERE email = '$email';");
	$result = mysqli_fetch_array($query);
	$usr = $result['username'];

	//invalid user
	if (!$result){
		echo('<center><span style="color:red;"><b>Invalid User ID</b></span></center>');
		mysqli_close($con);
		return false;
	}	

	//successful login
	else if (strcmp(md5($pw),trim($result['password']))==0) {
		session_regenerate_id();
			$_SESSION['username']=$usr;
			$_SESSION['email']=$email;
			$_SESSION['picURL']=$result['pictureURL'];
			$_SESSION['userID']=$result['id'];
			if ($result['admin'] == 1)
				$_SESSION['admin'] = true;
		session_write_close();
		mysqli_close($con);
		return true;
	}

	//invalid password
	echo('<center><span style="color:red;"><b>Invalid Password</b></span></center>');
	mysqli_close($con);
	return false;
}

function createClass($post){
	$courseNum = strtolower($post['courseNumber']); $courseSub = strtolower($post['courseSubject']); 
	$section = strtolower($post['section']); $prof = strtolower($post['prof']);

	//getting user from database
	$con = sql_connect();
	//search and see if class already exists
	$query = mysqli_query($con, "SELECT * FROM classes WHERE courseSubject = '$courseSub' AND courseNumber = '$courseNum' 
												AND section = '$section' AND professor = '$prof';");
	$result = mysqli_fetch_array($query);

	$classFormatted = strtoupper($courseSub) . $courseNum . '<br>Section #:' . $section . 
										'<br>Professor: ' . strtoupper(substr($prof, 0, 1)) . substr($prof, 1);

	//if no results, create a class
	if (!$result){
		mysqli_query($con, "INSERT INTO classes(courseSubject, courseNumber, section, professor) VALUES ('$courseSub', '$courseNum', '$section', '$prof');");
		echo('
			Class created: <br>' .
			$classFormatted .'<br><br>
			<button id="buttonStyle">Share This Class</button>
		');
		$classID = mysqli_fetch_array(mysqli_query($con, "SELECT id FROM classes WHERE courseSubject = '$courseSub' AND courseNumber = '$courseNum' AND section = '$section' AND professor = '$prof';"));
		addClass(intval($classID['id']));

	}

	//class already exists
	else{
		echo("This class already exists:<br>" . $classFormatted);
		echo("<br><a href='#'>Add This Class</a>");
	}

	mysqli_close($con);
}

//list classes for a user. Currently used on their home page
function getClasses($email){
	$con = sql_connect();
	$query = mysqli_query($con, "SELECT classes FROM users WHERE email = '$email';");
	$result = mysqli_fetch_array($query);

	$classes = explode(',', $result['classes']);
	if (count($classes) > 0){
		echo "
			<table id='classTable'>
				<thead>
					<th>Subject</th>
					<th>Number</th>
					<th>Section</th>
					<th>Professor</th>
					<th>Page</th>
				</thead>
				<tbody>
		";
	}

	for ($i=1;$i<count($classes)-1;$i++){
		$class = intval($classes[$i]);
		$query = mysqli_query($con, "SELECT * FROM classes WHERE id = '$class';");
		$result = mysqli_fetch_array($query);
		echo "
			<tr>
				<td>". strtoupper($result['courseSubject']) . "</td>
				<td>$result[courseNumber]</td>
				<td>$result[section]</td>	
				<td>" . strtoupper(substr($result['professor'], 0, 1)) . substr($result['professor'], 1) . "</td>
				<td><a href='view_class.php?classID=" . $result['id'] . "'><button id='buttonStyle2'>Go to Page</button></a></td>
			</tr>
		";
	}
	echo "</tbody></table>";
	mysqli_close($con);
}

//function that is called when a user registers
function registerUser($post){
    $con = sql_connect();
    $query = mysqli_query($con, "SELECT * FROM users WHERE email = '$post[email]';");
    $result = mysqli_fetch_array($query);
             
    if (!empty($result)){
        echo "<span style='color:red;'><b>Email already exists, please use a different one</b></span>";
        return false;
    }
    
    $emailSuffix = substr($post['email'], strpos($post['email'], '@'));
    if ($emailSuffix != "@seawolf.sonoma.edu"){
        echo "<span style='color:red;'><b>Seawolf Email Required</b></span>";
        return false;
    }
    
    $username = $post['fName'] . ',' . $post['lName'];
    $password = md5($post['password']);
    mysqli_query($con, "INSERT INTO users(username, password, email) VALUES ('$username','$password','$post[email]');");
    echo("
         <b>Account Created!<b><br>
         <a href='add_class.php'><button id='buttonStyle'>Let's Get Started!</button></a>
         ");

    mysqli_close($con);
}

//displays search and restuls
function searchClassHTML(){
	$html = "
		<form action='add_class.php' method='get' style='display:inline-block;'>
			<input type='text' name='department' value='$_GET[department]' placeholder='Department' style='width:90px;'>
			<input type='text' name='coureNum' value='$_GET[courseNum]' placeholder='Course #' style='width:90px;'>
			<input type='text' name='prof' value='$_GET[prof]' placeholder='Professor' style='width:90px;'>
			<input type='text' name='section' value='$_GET[section]' placeholder='Section' style='width:90px;'>
			<input type='submit' id='buttonStyle' value='Search' style='padding:5px'>
		</form>
		<a href='add_class.php' style='display:inline-block;'><button id='buttonStyle'>Reset</button></a>
	";

	$con = sql_connect();

	//search query, display results in table
	if (!empty($_GET)){
		$query = mysqli_query($con, "SELECT * FROM classes WHERE courseSubject LIKE '%$_GET[department]%' AND courseNumber LIKE 
													'%$_GET[courseNum]%' AND section LIKE '%$_GET[section]%' AND professor LIKE '%$_GET[prof]%'");

		if (mysqli_num_rows($query) > 0){
			$html .= "
				<table id='searchTable'>
					<thead>
						<th>Subject</th>
						<th>Number</th>
						<th>Section</th>
						<th>Professor</th>
						<th>Add</th>
					</thead>
					<tbody>
			";

			//get users classes - we'll use this because we don't want a user to be able to add a class they're already enrolled in
			$query2 = mysqli_query($con, "SELECT classes FROM users WHERE email = '$_SESSION[email]';");
			$result2 = mysqli_fetch_array($query2);
			$classes = explode(',', $result2['classes']);
		}

		while ($row = mysqli_fetch_array($query)){
			$html .= "
				<tr>
					<td height='30	'>". strtoupper($row['courseSubject']) . "</td>
					<td>$row[courseNumber]</td>
					<td>$row[section]</td>	
					<td>" . strtoupper(substr($row['professor'], 0, 1)) . substr($row['professor'], 1) . "</td>
					<td>
			";
			if (!in_array(strval($row['id']), $classes)){
				$html .= "
					<form action='add_class.php' method='post' style='margin:10px 0px;'>
						<input type='hidden' value='$row[id]' name='classID'>
						<input type='submit' value='Add this class' id='buttonStyle'>
					</form>
				";
			}
			else
				$html .= "You've already added this class";
			$html .= "</td></tr>";
		}
		$html .= "</tbody></table>";
	}
	echo $html;
	mysqli_close($con);
}

//when a user adds a class to their account
function addClass($id){
	$idFormatted = $id . ',';
	$idInt = intval($id);
	$con = sql_connect();
	//add the class 
	mysqli_query($con, "UPDATE users SET classes = CONCAT(classes, '$idFormatted') WHERE email = '$_SESSION[email]';");
	$query = mysqli_query($con, "SELECT courseSubject, courseNumber FROM classes WHERE id = '$idInt';");
	$result = mysqli_fetch_array($query);
	echo("
		<h3>" . strtoupper($result['courseSubject']) . ' ' . $result['courseNumber'] . " Added</h3><br>
		<a href='home.php'>Go Home</a> | <a href='add_class.php'>Browse More Classes</a>
	");
	mysqli_close($con);
}

//page that generates HTML for an individual class page
function classHTML(){
	$classID = intval($_GET['classID']);
	//grab the information of the linked class
	$con = sql_connect();
	$query = mysqli_query($con, "SELECT * FROM classes WHERE id = '$classID'");
	$result = mysqli_fetch_array($query);

	//not a valid class, redirect
	if (empty($result))
		echo ('<script type="text/javascript">window.location = "home.php";</script>');

	//header for page
	echo("<h1>" . strtoupper($result['courseSubject']) . " " . $result['courseNumber'] . " - Section " . $result['section'] . "</h1>");

	echo ("
		<!--<form action='home.php' method='post' style='position: relative; left: 810px;width:190px;'>
			<input type='hidden' name='removeClassID' value='$classID'>
			<input type='submit' id='buttonStyle' value='Remove Class'>
		</form>-->

		<div style='max-height:25px;'>
			<h3 style='width:320px;'>Students</h3>
			<h3 style='width:340px;position:relative;bottom:39px;left:288px;'>Discussion</h3>
			<button style='position:relative;bottom:75px;left:390px;' id='buttonStyle2' onclick='div_show()'>Add a topic</button>
		</div>
		<div id='studentList' style='float:left;'>
		<table>
	");

	//grab list of students enrolled
	$query2 = mysqli_query($con, "SELECT username, pictureURL FROM users WHERE classes LIKE '%," . $result['id'] . ",%';");

	//display enrolled students
	while ($student = mysqli_fetch_array($query2)){
		echo("
			<tr>
				<td><img src='img/profile/" . $student['pictureURL'] . "' style='width:80px;margin-right:10px;'></td>
				<td>
					<span>" . explode(',', $student['username'])[0] . " " . explode(',', $student['username'])[1] . "
						<br>	<a href='#' style='position:relative;bottom:10px;'>Pass Note</a>
						<img src='img/messageico.png'>
					</span>
				</td>
			</tr>
		");
	}
	echo("
		</table>
		</div>
		<div id='topicList'>
		<table>
	");
			
	//grabbing topics
	$query3 = mysqli_query($con, "SELECT * FROM topics WHERE classID=" . intval($_GET['classID']) . " ORDER BY lastUpdated DESC");
	
	//displaying topics
	while ($topic = mysqli_fetch_array($query3)){
		//get topic owner info
		$topicOwner = mysqli_fetch_array(mysqli_query($con, "SELECT username, pictureURL FROM users WHERE id=" . $topic['topicOwner']));
		//get number of replies for each topic
		$replyNumber = mysqli_num_rows(mysqli_query($con, "SELECT id FROM replies WHERE topicID=" . $topic['id'] .";"));
		echo("
			<tr>
				<td><img src='img/profile/" . $topicOwner['pictureURL'] . "' style='width:60px;'></td>
				<td valign='top' style='width:150px;'>" 
					. explode(',', $topicOwner['username'])[0] . " " . explode(',', $topicOwner['username'])[1] . "<br><small>"
					. $topic['date'] . "</small>" . "<br><small>Replies: " . $replyNumber ."<br><a href='view_post.php?topicID=" . $topic['id'] . "'>View/Comment</a>" .
				"</td>
				<td>" . $topic['topicContent'] ." </td>
			</tr>
		");
	}
	
	echo("		
		</table>
		</div>
	");
	mysqli_close($con);
}

//when a user enters a new topic this function handles submitting that data into the database
function newPost($classID){
	$con = sql_connect();
	mysqli_query($con, "INSERT INTO topics(topicContent, date, classID, topicOwner, lastUpdated) 
							VALUES('" . mysql_real_escape_string($_POST['newPost']) . "', '" . date('Y-m-d H:i:s') . "', '$classID', " . 
							intval($_SESSION['userID']) . ", '" . date('Y-m-d H:i:s') . "');");
	mysqli_close($con);
}

function postHTML(){
	$topicID = intval($_GET['topicID']);
	//grab the information of the topic
	$con = sql_connect();
	$query = mysqli_query($con, "SELECT * FROM topics JOIN users ON users.id=topics.topicOwner WHERE topics.id = $topicID");
	$topicData = mysqli_fetch_array($query);

	echo("
		<a href='view_class.php?classID=" . $topicData['classID'] . "'>Return to Session</a>
		<div style='max-height:285px; overflow:auto; margin-top:10px;'>
			<table>
				<tr>
					<td style='width:120px;'><img src='img/profile/" . $topicData['pictureURL'] . "' style='width:100px;margin-right:15px;'></td>
					<td valign='top' style='padding-right:10px;' style='width:200px;'><h3>"	
						. explode(',', $topicData['username'])[0] . " " . explode(',', $topicData['username'])[1] . "</h3>
						<small>" . $topicData['date'] . "</small> 
					</td>" . 
					"<td style='width:705px;'>" . $topicData['topicContent'] . "</td>
				</tr>
	");

	$query = mysqli_query($con, "SELECT replies.id,  users.pictureURL, users.username, replies.date, replies.replyContent, 
		replies.replyOwner, replies.helpful FROM replies JOIN users ON users.id=replies.replyOwner WHERE replies.topicID= $topicID 
		ORDER BY replies.date ASC");

	while($reply = mysqli_fetch_array($query)){
		$cellStyle = ""; $helpfulCount = "";
		echo("
			<tr>
				<td style='width:120px;'><img src='img/profile/" . $reply['pictureURL'] . "' style='width:100px;margin-right:15px;'></td>
				<td valign='top' style='padding-right:10px;' style='width:200px;'><h3>"	
					. explode(',', $reply['username'])[0] . " " . explode(',', $reply['username'])[1] . "</h3>
					<small>" . $reply['date'] . "</small> 
				");


			//edit/delete comment if comment owner or is an admin
			if ($_SESSION['userID'] == $reply['replyOwner'] || $_SESSION['admin']){
				echo("
					<br>
					<form action='view_post.php?topicID=" . $topicID . "' method='post'>
						<input type='hidden' name='replyID' value='" . $reply['id'] ." '>
						<input type='submit' name='editPost' value='Edit'>
						<input type='submit' name='deletePost' value='Delete'
							onClick=\"return confirm('Are you sure you want to delete your comment?')\">
					</form>
				");
			}
			if (intval($reply['helpful']) > 0){
				$cellStyle = 'border:6px solid #0000B2;';
				$helpfulCount = '<br>This post was found helpful.';
			}
			echo("
				<small>
					Was this post helpful?<br>
					<a href='#'><img src='img/goodThumbSmall.png'></a>&nbsp;&nbsp;<a href='#'><img src='img/badThumbSmall.png'></a>"
					. $helpfulCount . "
				</small>
				</td><td style='width:705px;" . $cellStyle . "'>" . $reply['replyContent'] . "</td>
			</tr>
		");
	}

	//add a reply
	echo("
		</table>
		</div>
		<h3>Leave A Comment</h3>
		<img src='img/profile/" . $_SESSION['picURL'] . "' style='width:100px;float:left;margin-left: 3px;'>
		<form action='view_post.php?topicID=" . $topicID . "' method='post' style='width: 90%; float: left;'>
			<textarea name='reply' style='height: 90px; margin-left: 10px; width: 90%;'></textarea>
			<input type='submit' value='Post'>
		</form>
	");

	mysqli_close($con);
}

function submitReply(){
	$con = sql_connect();
	//add reply to DB
	mysqli_query($con, "INSERT INTO replies(replyContent, date, topicID, replyOwner) VALUES('" . mysql_real_escape_string($_POST['reply']) . 
							"', '" . date('Y-m-d H:i:s') . "', '" . $_GET['topicID'] . "', '" . $_SESSION['userID'] . "');");
	//update the topic's last touched field
	mysqli_query($con, "UPDATE topics SET lastUpdated='" . date('Y-m-d H:i:s') . "' WHERE id='" . 
							$_GET['topicID'] . "';");
	mysqli_close($con);
}

function deletePost(){
	$con = sql_connect();
	mysqli_query($con, "DELETE FROM replies WHERE id='" . $_POST['replyID'] . "';");
	mysqli_close($con);
}

//TODO
function removeClass(){
	$con = sql_connect();
	$query = mysqli_query($con, "SELECT classes FROM users WHERE id='" . $_SESSION['userID'] . "';");
	$classes = mysqli_fetch_array($query)['classes'];
//	echo $classes;
}

?>
