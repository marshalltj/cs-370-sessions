<?php
    include 'includes/header.php';
    include 'includes/functions.inc.php';
	//if we're not logged in redirect
	if (empty($_SESSION))
		echo ('<script type="text/javascript">window.location = "login.php";</script>');
	//if we're not displaying a class redirect
	else if (empty($_GET))
		echo ('<script type="text/javascript">window.location = "home.php";</script>');

	//if they're adding a topic
	if (isset($_POST['newPost'])){
		newPost($_GET['classID']);
	}
?>
<script src="js/main.js"></script>
<div id='content' style='min-height:470px;'>
	<?php classHTML();?>
</div>

<!--Popup Form-->
<div onClick="check(event)">
	<div id="popupContainer">
		<!-- Popup Div Starts Here -->
		<div id="popupWindow">
			<!-- Contact Us Form -->
			<?php echo"<form action='view_class.php?classID=" . $_GET['classID'] . "'id='popupForm' method='post' name='form'>"; ?>
				<img id="close" src="img/icon_close.png">
				<h2>Add Topic</h2>
				<hr>
				<h3>Enter Message</h3>
				<textarea id="msg" name="newPost"></textarea>
				<a href="javascript:%20check_empty()" id="postSubmit">Post</a>
			</form>
		</div>
	</div>
</div>

<?php include'includes/footer.php';?>
